<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    //
    public function index(Request $request)
    {
        $data = Product::all();
        return response()->json(['error' => false, 'data' => $data], 200);
    }
}
